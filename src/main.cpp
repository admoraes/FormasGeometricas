#include "../inc/figuraGeometrica.hpp"

#include <iostream>

using namespace std;

int main()
{
	int tipoFigura;

	FiguraGeometrica circulo;
	FiguraGeometrica paralelogramo;

	cout << "Selecione o tipo de area desejada: " << endl;
	cout << "1 - Area do circulo" << endl;
	cout << "2 - Area de um paralelogramo" << endl; //quadrados e retangulos são paralelogramos
	cin >> tipoFigura;

	switch(tipoFigura)
	{
		case 1:
			float raio;
			cout << "Qual o raio do circulo? :";
			cin >> raio;
			circulo.calcularArea(raio);
			cout << "A area do circulo e: " << circulo.getArea() << endl;
			break;
		case 2:
			float base, altura;
			cout << "Qual a altura do paralelogramo? :";
			cin >> altura;
			cout << "Qual a base do paralelogramo? :";
			cin >> base;
			paralelogramo.calcularArea(base, altura);
			cout << "A area do paralelogramo e: " << paralelogramo.getArea() << endl;
			break;

	}

	return 0;
}
