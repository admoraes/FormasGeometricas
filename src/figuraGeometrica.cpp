#include "../inc/figuraGeometrica.hpp"

#define PI 3.14159

FiguraGeometrica::FiguraGeometrica()
{
}

FiguraGeometrica::~FiguraGeometrica()
{
}

void FiguraGeometrica::calcularArea(float raio)
{
	float area;
	area = PI*raio*raio;
	this->area = area;
}

void FiguraGeometrica::calcularArea(float base, float altura)
{
	float area;
	area = base*altura;
	this->area =  area;
}

float FiguraGeometrica::getArea()
{
	return this->area;
}
