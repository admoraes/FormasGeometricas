#ifndef FIGURA_GEOMETRICA_HPP
#define FIGURA_GEOMETRICA_HPP


class FiguraGeometrica
{
	private:
	float area;

	public:
	FiguraGeometrica();
	~FiguraGeometrica();

	void calcularArea(float raio);
	void calcularArea(float base, float altura);

	float getArea();
};

#endif
